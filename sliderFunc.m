function sliderFunc(handles)

hax = handles.noteAxes;
axis (handles.noteAxes,[handles.noteSlider.Value, handles.noteSlider.Value + 100, -30, 30]);
a = axis;
yStart = 7;
ySpace = 4;
% treble clef staff lines
for iy = yStart:ySpace:ySpace*4+yStart
    line(a(1:2),[iy,iy])
end
% bass clef staff lines
yBStart = -20;
for iy = yBStart:ySpace:ySpace*4+yBStart
    line(a(1:2),[iy,iy])
end
