function plotPracNotesF(handles)
% Called upon opening of the GUI
% Reads in and places the treble clef and bass clef images
% Draws the staff lines

%% Place treble clef image and bass clef image

[Tclef, mapT, CTalpha] = imread('TrebleClef.png'); % read in the image
image(Tclef, 'AlphaData', CTalpha); % draws image and makes background transparent

% turn image right side up
for i = 1:3
    Tclef(:,:,i) = flipud(Tclef(:,:,i));
end
CTalpha = flipud(CTalpha);

hax = handles.noteAxes; % get current axis and store in axis handle

[Bclef, mapB, CBalpha] = imread('BassClef.png'); % read in the image
image(Bclef, 'AlphaData', CBalpha); % draws image and makes background transparent

% turn image right side up
for i = 1:3
    Bclef(:,:,i) = flipud(Bclef(:,:,i));
end
CBalpha = flipud(CBalpha);

axis (hax, 'xy') % set to cartesian coordinates
axis (hax, 'equal')
axis (handles.noteAxes,[handles.noteSlider.Value, handles.noteSlider.Value + 100, -30, 30]);
a = axis;
axis (hax, 'off')
hold (hax, 'on') % so that everything plots on the same axes instead of plotting over existing things (making them no longer visible)

%% Draw staff lines
yStart = 7;
ySpace = 4;

% treble clef staff lines
for iy = yStart:ySpace:ySpace*4+yStart
    line(a(1:2),[iy,iy])
end

% bass clef staff lines
yBStart = -20;
for iy = yBStart:ySpace:ySpace*4+yBStart
    line(a(1:2),[iy,iy])
end


%% Place treble and bass clef images
hCimage = image('CData', Tclef, 'AlphaData', CTalpha); % save image and alpha (background) data in image object
hCimage.Parent = hax; % makes image a child of the current axes

% place treble clef
[Tcy, Tcx, Tcz] = size(Tclef);
TclefHeight = 22;
TclefWidth = TclefHeight*(Tcx/Tcy);
TxLL = 2;
TxUR = TxLL + TclefWidth;
TyLL = 2.75;
TyUR = TyLL + TclefHeight;

hCimage.XData = [TxLL, TxUR];
hCimage.YData = [TyLL, TyUR];

hCBimage = image('CData', Bclef, 'AlphaData', CBalpha); % save image and alpha (background) data in image object
hCBimage.Parent = hax; % makes image a child of the current axes

% place bass clef
[Bcy, Bcx, Bcz] = size(Bclef);
BclefHeight = 12;
BclefWidth = BclefHeight*(Bcx/Bcy);
BxLL = 5;
BxUR = BxLL + BclefWidth;
ByLL = -16.5;
ByUR = ByLL + BclefHeight;

hCBimage.XData = [BxLL, BxUR];
hCBimage.YData = [ByLL, ByUR];
hold (hax, 'off')

