function varargout = MusicMasterv1(varargin)
% MUSICMASTERV1 MATLAB code for MusicMasterv1.fig
%      MUSICMASTERV1, by itself, creates a new MUSICMASTERV1 or raises the existing
%      singleton*.
%
%      H = MUSICMASTERV1 returns the handle to a new MUSICMASTERV1 or the handle to
%      the existing singleton*.
%
%      MUSICMASTERV1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MUSICMASTERV1.M with the given input arguments.
%
%      MUSICMASTERV1('Property','Value',...) creates a new MUSICMASTERV1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MusicMasterv1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MusicMasterv1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MusicMasterv1

% Last Modified by GUIDE v2.5 19-Apr-2016 10:43:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MusicMasterv1_OpeningFcn, ...
                   'gui_OutputFcn',  @MusicMasterv1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MusicMasterv1 is made visible.
function MusicMasterv1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MusicMasterv1 (see VARARGIN)

% Choose default command line output for MusicMasterv1
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MusicMasterv1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);

plotPracNotesF(handles)

% --- Outputs from this function are returned to the command line.
function varargout = MusicMasterv1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function scrollSlider_Callback(hObject, eventdata, handles)
% hObject    handle to scrollSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

plotPracNotesF(handles)
disp(handles.scrollSlider.Value);
% --- Executes during object creation, after setting all properties.
function scrollSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scrollSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on button press in harmCheckbox.
function harmCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to harmCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of harmCheckbox


% --- Executes on button press in playButton.
function playButton_Callback(hObject, eventdata, handles)
% hObject    handle to playButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Fs = 44100;
% for michael = 1:str2double(handles.noteNumberText.String)
sound(abs(fullrecording), Fs);
% pause(str2double(handles.durationText.String))


% --- Executes on button press in recordButton.
function recordButton_Callback(hObject, eventdata, handles)
% hObject    handle to recordButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp('Button Pressed')

function recordText_Callback(hObject, eventdata, handles)
% hObject    handle to recordText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of recordText as text
%        str2double(get(hObject,'String')) returns contents of recordText as a double


% --- Executes during object creation, after setting all properties.
function recordText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to recordText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function noteNumberText_Callback(hObject, eventdata, handles)
% hObject    handle to noteNumberText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of noteNumberText as text
%        str2double(get(hObject,'String')) returns contents of noteNumberText as a double


% --- Executes during object creation, after setting all properties.
function noteNumberText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noteNumberText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function durationText_Callback(hObject, eventdata, handles)
% hObject    handle to durationText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of durationText as text
%        str2double(get(hObject,'String')) returns contents of durationText as a double


% --- Executes during object creation, after setting all properties.
function durationText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to durationText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in recordingButton.
function recordingButton_Callback(hObject, eventdata, handles)
% hObject    handle to recordingButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Record/listen

fundamentalFreq = zeros(1, str2double(handles.noteNumberText.String));
Fs = 44100;
myRecording = {1, str2double(handles.noteNumberText.String)};
myRecObj = audiorecorder(Fs, 16, 2);
handles.statusText.String = 'Initializing.';
pause(.5);
recordblocking(myRecObj, str2double(handles.durationText.String));
for it = 1:str2double(handles.noteNumberText.String)
    myRecObj = audiorecorder(Fs, 16, 2);
    handles.statusText.String = 'Start playing.';
    pause(.5);
    recordblocking(myRecObj, str2double(handles.durationText.String));
    handles.statusText.String = 'Pause.';
    pause(.5);
    myRecording{it} = getaudiodata(myRecObj);
 
    %% Run FFT
    Ts = 1/Fs;
    N = length(myRecording{it});
    T = N * Ts;
    f = 0:1/T:(N/2 - 1)/T;
    spec = fft(myRecording{it});
    magnitude = abs(spec);
    mags = magnitude(:, 1);
    
    [maxM, position] = max(mags); % finds the loudest frequency and its position in the array
    maxFrequency(it) = f(position); % finds the corresponding frequency of the loudest spectral component
    
    % saves the frequencies and their corresponding magnitudes in an array
    % called MandF (used for debugging, but not necessary)

    
    %% Account for harmonics
    
    % note: The fundamental frequency is the frequency of the pitch played.
    % Harmonics are integer multiples of the fundamental frequency. Sometimes a
    % harmonic will be detected with stronger magnitude than the fundamental
    % frequency, so this must be accounted for. Frequencies lower than the
    % fundamental frequency will not be present. For example, if a 440 Hz A was
    % played, it is possible to have an 880 Hz component appear in the FFT, but a 110 Hz component should not appear.
    
    smoothmags = smooth(mags); % smooth out the mags vector to eliminate unimportant peaks
    smoothmags2 = smoothmags.'; % flip from a single column to a single row
    found = false; % initialize boolean for while loop
    fundPos2 = round(maxFrequency(it)/2); % loudest frequency was first harmonic
    fundPos3 = round(maxFrequency(it)/3); % loudest frequency was second harmonic
    fundPos4 = round(maxFrequency(it)/4); % loudest frequency was third harmonic
    [peaks, locations, widths, prominence] = findpeaks(smoothmags2(1:length(f)), f); % returns the height of peaks, their locations (frequencies they occur at), their widths, and how prominent each peak is (its height)
    minPeakProm = 18; % minimum peak prominence required to consider the peak to be significant
    icount = 1; % initialize counter
    
    while ~found
        locRound = round(locations(icount)); % all numbers must be rounded so they can be compared in if conditions
        promRound = round(prominence(icount));
        if(locRound >= fundPos2-3) && (locRound <= fundPos2 + 3)% if any peaks occur at half the max frequency
            if(promRound > minPeakProm) % if the peak was prominent enough to be considered
                fundamentalFreq(it) = maxFrequency(it)/2; % the fundamental frequency (pitch played) was half the maxFrequency detected
                found = true; % allows while loop to be exited
            end
        elseif(locRound >= fundPos3+3) && (locRound <= fundPos3 - 3)
            if(promRound > minPeakProm)
                fundamentalFreq(it) = maxFrequency(it)/3;
                found = true;
            end
        elseif (locRound >= fundPos4 - 3) && (locRound <= fundPos4 + 3)
            if(promRound > minPeakProm)
                fundamentalFreq(it) = maxFrequency(it)/4;
                found = true;
            end
        elseif icount == length(locations) % enter this section one all peaks have been checked
            fundamentalFreq(it) = maxFrequency(it); % if none of the above occur, the max frequency is the fundamental frequency
            found = true;
        end
        icount = icount+1; % move to the next peak and check it
    end
end

handles.statusText.String = 'End Recording';

%% Place treble clef image and bass clef image

[Tclef, mapT, CTalpha] = imread('TrebleClef.png'); % read in the image
image(Tclef, 'AlphaData', CTalpha); % draws image and makes background transparent

% turn image right side up
for i = 1:3
    Tclef(:,:,i) = flipud(Tclef(:,:,i));
end
CTalpha = flipud(CTalpha);

hax = handles.noteAxes; % get current axis and store in axis handle

[Bclef, mapB, CBalpha] = imread('BassClef.png'); % read in the image
image(Bclef, 'AlphaData', CBalpha); % draws image and makes background transparent

% turn image right side up
for i = 1:3
    Bclef(:,:,i) = flipud(Bclef(:,:,i));
end
CBalpha = flipud(CBalpha);

axis (hax, 'xy') % set to cartesian coordinates
axis (hax, 'equal')
axis (hax, [0, 100, -30, 30]);
a = axis;
axis (hax, 'off')
hold (hax, 'on') % so that everything plots on the same axes instead of plotting over existing things (making them no longer visible)

%% Draw staff lines
yStart = 7;
ySpace = 4;

% treble clef staff lines
for iy = yStart:ySpace:ySpace*4+yStart
    line(a(1:2),[iy,iy])
end

% bass clef staff lines
yBStart = -20;
for iy = yBStart:ySpace:ySpace*4+yBStart
    line(a(1:2),[iy,iy])
end

%% Caculate note positions and plot

x(1)=22; % x-coordinate of first note plotted

for iplot = 1:length(fundamentalFreq)
frequency = fundamentalFreq(iplot); % indexes through frequencies in the vector passed to it from recording function
baseFreq1 = 261.63; % C4
n1(iplot) = round(log10(frequency/baseFreq1)/log10(2^(1/12))); % compare to C4, n1 is the number of half-steps away from the base pitch
even = mod(n1(iplot),2);   % divides (Even  number returns 0, odd returns 1)

baseIndex = 4; % index of the base frequency used
Index1(iplot) = floor(n1(iplot)/12+baseIndex); % determines the index of the frequency using the base frequency and the number of half-steps away from the base frequency (n1)

if even == 0 % for even n
    increment = 2*(Index1(iplot)-baseIndex)+n1(iplot);
else % for odd n
    baseFreq2 = 329.63; % E4
    n2 = round(log10(frequency/baseFreq2)/log10(2^(1/12))); % compare to E4
    odd = mod(n2,2);   % divides (Even  number returns 0, odd returns 1)
    if odd == 1 % odd n compared to E
        Index2 = floor(n2/12); % treats E(N) through D sharp (N+1) as having index N-4 because increment is calculated in the same way on these intervals
        increment = 1 + Index2*2 + n1(iplot);
    end
end

middleC = 3; % y-position of middle C
yposition(iplot) = middleC + increment; % y-position of the frequency, based on middle C's y-position because middle C is the (main/initial) base frequency used

if yposition(iplot) < 3 % notes below middle C
    yposition(iplot) = yposition(iplot)-3; % shift down 3 units to compensate for extra room/space between the two staffs
end

if yposition(iplot) == 3 % middle C
    yposition(iplot) = 1.5; % halfway between the two staffs
    line([x(iplot)-3, x(iplot)+3],[yposition(iplot), yposition(iplot)]) % draws a ledger line through middle C
end

x(iplot+1) = x(iplot) + 10; % increment x-value so they are in order and not on top of each other

end

plot(hax,x(1:length(yposition)), yposition, 'ko', 'MarkerFaceColor', 'k', 'MarkerSize', 10); % plots all notes initially in black in correct positions

% Check for sharps
Sharps = [1 3 6 8 10]; % n-value of sharps in the octave of the base index
for icheck = 1:length(fundamentalFreq); % steps through n-values of each frequency
% for icount = 1:length(yposition)
%     nSharp(icount) = n1(icheck)-12*(Index1(icheck) - baseIndex);
nSharp(icheck) = n1(icheck)-12*(Index1(icheck) - baseIndex);
    for runSharps = 1:length(Sharps) % steps through the Sharps vector
        if nSharp(icheck) == Sharps(runSharps) % compares new n value to n values in Sharps vector to see if they match (the frequency corresponds to a sharp)
            plot(hax,x(icheck), yposition(icheck), 'bo', 'MarkerFaceColor', 'b', 'MarkerSize', 10); % plots new note in same position on top of existing note, but in blue
        end
    end
end
% end
hold (hax, 'off')

%% Place treble and bass clef images
hCimage = image('CData', Tclef, 'AlphaData', CTalpha); % save image and alpha (background) data in image object
hCimage.Parent = hax; % makes image a child of the current axes

% place treble clef
[Tcy, Tcx, Tcz] = size(Tclef);
TclefHeight = 22;
TclefWidth = TclefHeight*(Tcx/Tcy);
TxLL = 2;
TxUR = TxLL + TclefWidth;
TyLL = 2.75;
TyUR = TyLL + TclefHeight;

hCimage.XData = [TxLL, TxUR];
hCimage.YData = [TyLL, TyUR];

hCBimage = image('CData', Bclef, 'AlphaData', CBalpha); % save image and alpha (background) data in image object
hCBimage.Parent = hax; % makes image a child of the current axes

% place bass clef
[Bcy, Bcx, Bcz] = size(Bclef);
BclefHeight = 12;
BclefWidth = BclefHeight*(Bcx/Bcy);
BxLL = 5;
BxUR = BxLL + BclefWidth;
ByLL = -16.5;
ByUR = ByLL + BclefHeight;

hCBimage.XData = [BxLL, BxUR];
hCBimage.YData = [ByLL, ByUR];

if handles.playCheckbox.Value
    Fs = 44100;
    for michael = 1:str2double(handles.noteNumberText.String)
        sound(myRecording{michael}, Fs);
        pause(str2double(handles.durationText.String))
    end
end

% --- Executes on slider movement.
function noteSlider_Callback(hObject, eventdata, handles)
% hObject    handle to noteSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
sliderFunc(handles)

% --- Executes during object creation, after setting all properties.
function noteSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noteSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in playCheckbox.
function playCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to playCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of playCheckbox
